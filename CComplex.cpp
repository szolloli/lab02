#include "CComplex.h"
#include <iostream>
#include <math.h>


CComplex::CComplex(float real, float imaginary)
{
	r = real;
	i = imaginary;
}

void CComplex::print()
{
	if (r && i)
	{
		if (i < 0)
			std::cout << r << i << "j" << std::endl;
		else
			std::cout << r << "+" << i << "j" << std::endl;
	}
	else if (r != 0.0f)
	{
		std::cout << r << std::endl;
	}
	else if (i)
	{
		if (i == 1.0f)
			std::cout << "j" << std::endl;
		else if (i == -1.0f)
			std::cout << "-j" << std::endl;
		else
			std::cout << i << "j" << std::endl;
	}
	else
	{
		std::cout << 0 << std::endl;
	}
	
}

CComplex operator + (CComplex const& c1, CComplex const& c2)
{
	return CComplex(c1.r + c2.r, c1.i + c2.i);
}

CComplex operator - (CComplex const& c1, CComplex const& c2)
{
	return CComplex(c1.r - c2.r, c1.i - c2.i);
}

CComplex operator * (CComplex const& c1, CComplex const& c2)
{
	return CComplex(c1.r * c2.r - c1.i * c2.i, c1.r * c2.i + c1.i * c2.r);
}

CComplex operator / (CComplex const& c1, CComplex const& c2)
{
	return CComplex((c1.r * c2.r + c1.i * c2.i) / (pow(c2.r, 2) + pow(c2.i, 2)), (c1.i * c2.r - c1.r * c2.i) / (pow(c2.r, 2) + pow(c2.i, 2)));
}

bool operator == (CComplex const& c1, CComplex const& c2)
{
	return (c1.r == c2.r && c1.i == c2.i);
}

bool operator != (CComplex const& c1, CComplex const& c2)
{
	return (c1.r != c2.r || c1.i != c2.i);
}

