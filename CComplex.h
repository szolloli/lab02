#include <iostream>

class CComplex {
public:
	float r, i;
	CComplex(float real = 0.0f, float imaginary = 0.0f);
	void print();
};
