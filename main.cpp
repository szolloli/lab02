#include <iostream>
#include "CComplex.cpp"



using namespace std;



int main()
{	
	// print function test
	CComplex a(1,1), b(0,0), c(1,0), d(0,1), e(1,-2), f(0, -1);
	a.print();
	b.print();
	c.print();
	d.print();
	e.print();
	f.print();

	// / operator test
	CComplex g(3,-1), h(2,-2);
	a = g/h;
	a.print();
	a = g*h;
	a.print();
	a = g+h;
	a.print();
	a = g-h;
	a.print();
	cout << (int)(g != h) << endl;
	cout << (int)(g == h) << endl;
	return 0;
}
